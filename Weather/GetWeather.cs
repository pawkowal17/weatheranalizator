﻿using Newtonsoft.Json;
using System.Net;
using System.Text;
using Weather.Models.WeatherModels;

namespace Weather
{
    public class GetWeather
    {
        private readonly string _town;

        public GetWeather(string town)
        {
            _town = town;
        }

        public RootObject ConvertJson()
        {
            string json = GetJson();
            var root = JsonConvert.DeserializeObject<RootObject>(json);
            return root;
        }

        private string GetJson()
        {
            using (var webclient = new WebClient())
            {
                webclient.Encoding = Encoding.UTF8;
                var url = "http://api.openweathermap.org/data/2.5/weather?id=" + _town + "&APPID=1492a717dfe22b8351ade27c0b03a53b";
                var json = webclient.DownloadString(url);
                return json;
            }
        }
    }
}
