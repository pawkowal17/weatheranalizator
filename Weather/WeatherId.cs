﻿using System.Collections.Generic;

namespace Weather
{
    public class WeatherId
    {
        private readonly List<int> WeatherIds;

        public WeatherId()
        {
            WeatherIds = new List<int>
            {
                3081368,
                3102014,
                3092931,
                3098722,
                3093133,
                3094802,
                756135,
                3090048,
                7530819,
                858789,
                7531002,
                3096472,
                769250,
                763166,
                7530858,
                3083829
            };
        }

        public int FindById(int id)
        {
            return WeatherIds[id - 1];
        }
    }
}
