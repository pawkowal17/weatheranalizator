namespace Weather.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Quantities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Results",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuantitytId = c.Int(nullable: false),
                        StationId = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Value = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Quantities", t => t.QuantitytId, cascadeDelete: true)
                .ForeignKey("dbo.Stations", t => t.StationId, cascadeDelete: true)
                .Index(t => t.QuantitytId)
                .Index(t => t.StationId);
            
            CreateTable(
                "dbo.Stations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Town = c.String(),
                        Province = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Results", "StationId", "dbo.Stations");
            DropForeignKey("dbo.Results", "QuantitytId", "dbo.Quantities");
            DropIndex("dbo.Results", new[] { "StationId" });
            DropIndex("dbo.Results", new[] { "QuantitytId" });
            DropTable("dbo.Stations");
            DropTable("dbo.Results");
            DropTable("dbo.Quantities");
        }
    }
}
