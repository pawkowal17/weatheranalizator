﻿using System;
using Weather.Models;

namespace Weather
{
    public class DataResults
    {
        public void AddResults()
        {
            var weatherid = new WeatherId();

            using (var context = new WeatherDbContext())
            {
                foreach (var station in context.Stations)
                {
                    var id = weatherid.FindById(station.Id);
                    var weather = new GetWeather(id.ToString());
                    var root = weather.ConvertJson();

                    Result result;

                    result = new Result() { QuantitytId = 1, StationId = station.Id, Date = DateTime.Now, Value = (int)root.main.temp };
                    context.Results.Add(result);

                    result = new Result() { QuantitytId = 2, StationId = station.Id, Date = DateTime.Now, Value = root.clouds.all };
                    context.Results.Add(result);

                    result = new Result() { QuantitytId = 3, StationId = station.Id, Date = DateTime.Now, Value = (int)root.main.pressure };
                    context.Results.Add(result);

                    result = new Result() { QuantitytId = 4, StationId = station.Id, Date = DateTime.Now, Value = root.main.humidity };
                    context.Results.Add(result);

                    result = new Result() { QuantitytId = 5, StationId = station.Id, Date = DateTime.Now, Value = (int)root.wind.speed };
                    context.Results.Add(result);
                }
                context.SaveChanges();
            }
        }

        public void SeedManual()
        {
            using (var context = new WeatherDbContext())
            {
                Quantity quantity;

                quantity = new Quantity() { Name = "Temperatura", Type = "K" };
                context.Quantities.Add(quantity);

                quantity = new Quantity() { Name = "Zachmurzenie", Type = "%" };
                context.Quantities.Add(quantity);

                quantity = new Quantity() { Name = "Ciśnienie", Type = "hPa" };
                context.Quantities.Add(quantity);

                quantity = new Quantity() { Name = "Wilgotność", Type = "%" };
                context.Quantities.Add(quantity);

                quantity = new Quantity() { Name = "Prędkość wiatru", Type = "m/s" };
                context.Quantities.Add(quantity);

                Station station;

                station = new Station() { Town = "Wrocław", Province = "dolnośląskie" };
                context.Stations.Add(station);

                station = new Station() { Town = "Bydgoszcz", Province = "kujawsko-pomorskie" };
                context.Stations.Add(station);

                station = new Station() { Town = "Lubin", Province = "lubelskie" };
                context.Stations.Add(station);

                station = new Station() { Town = "Gorzów Wielkopolski", Province = "lubuskie" };
                context.Stations.Add(station);

                station = new Station() { Town = "Łódź", Province = "łódzkie" };
                context.Stations.Add(station);

                station = new Station() { Town = "Kraków", Province = "małopolskie" };
                context.Stations.Add(station);

                station = new Station() { Town = "Warszawa", Province = "mazowieckie" };
                context.Stations.Add(station);

                station = new Station() { Town = "Opole", Province = "opolskie" };
                context.Stations.Add(station);

                station = new Station() { Town = "Rzeszów", Province = "podkarpackie" };
                context.Stations.Add(station);

                station = new Station() { Town = "Białystok", Province = "podlaskie" };
                context.Stations.Add(station);

                station = new Station() { Town = "Gdańsk", Province = "pomorskie" };
                context.Stations.Add(station);

                station = new Station() { Town = "Katowice", Province = "śląskie" };
                context.Stations.Add(station);

                station = new Station() { Town = "Kielce", Province = "świętokrzyskie" };
                context.Stations.Add(station);

                station = new Station() { Town = "Olsztyn", Province = "warmińsko-mazurskie" };
                context.Stations.Add(station);

                station = new Station() { Town = "Poznań", Province = "wielkopolskie" };
                context.Stations.Add(station);

                station = new Station() { Town = "Szczecin", Province = "zachodniopomorskie" };
                context.Stations.Add(station);

                context.SaveChanges();
            }
        }
    }
}
