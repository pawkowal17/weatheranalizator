﻿using System.Data.Entity;
using Weather.Models;

namespace Weather
{
    public class WeatherDbContext : DbContext
    {
        public WeatherDbContext() : base("WeatherDatabase")
        {
            Database.SetInitializer(new WeatherDbInitializer());
        }

        public DbSet<Quantity> Quantities { get; set; }
        public DbSet<Station> Stations { get; set; }
        public DbSet<Result> Results { get; set; }
    }
}
