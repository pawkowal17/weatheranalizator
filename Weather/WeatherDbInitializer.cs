﻿using System.Data.Entity;
using Weather.Models;

namespace Weather
{
    public class WeatherDbInitializer : DropCreateDatabaseIfModelChanges<WeatherDbContext>
    {
        protected override void Seed(WeatherDbContext context)
        {
            Quantity quantity;

            quantity = new Quantity() { Name = "Temperatura", Type = "K" };
            context.Quantities.Add(quantity);

            quantity = new Quantity() { Name = "Zachmurzenie", Type = "%" };
            context.Quantities.Add(quantity);

            quantity = new Quantity() { Name = "Ciśnienie", Type = "hPa" };
            context.Quantities.Add(quantity);

            quantity = new Quantity() { Name = "Wilgotność", Type = "%" };
            context.Quantities.Add(quantity);

            quantity = new Quantity() { Name = "Prędkość wiatru", Type = "m/s" };
            context.Quantities.Add(quantity);

            Station station;

            station = new Station() { Town = "Wrocław", Province = "dolnośląskie" };
            context.Stations.Add(station);

            station = new Station() { Town = "Bydgoszcz", Province = "kujawsko-pomorskie" };
            context.Stations.Add(station);

            station = new Station() { Town = "Lubin", Province = "lubelskie" };
            context.Stations.Add(station);

            station = new Station() { Town = "Gorzów Wielkopolski", Province = "lubuskie" };
            context.Stations.Add(station);

            station = new Station() { Town = "Łódź", Province = "łódzkie" };
            context.Stations.Add(station);

            station = new Station() { Town = "Kraków", Province = "małopolskie" };
            context.Stations.Add(station);

            station = new Station() { Town = "Warszawa", Province = "mazowieckie" };
            context.Stations.Add(station);

            station = new Station() { Town = "Opole", Province = "opolskie" };
            context.Stations.Add(station);

            station = new Station() { Town = "Rzeszów", Province = "podkarpackie" };
            context.Stations.Add(station);

            station = new Station() { Town = "Białystok", Province = "podlaskie" };
            context.Stations.Add(station);

            station = new Station() { Town = "Gdańsk", Province = "pomorskie" };
            context.Stations.Add(station);

            station = new Station() { Town = "Katowice", Province = "śląskie" };
            context.Stations.Add(station);

            station = new Station() { Town = "Kielce", Province = "świętokrzyskie" };
            context.Stations.Add(station);

            station = new Station() { Town = "Olsztyn", Province = "warmińsko-mazurskie" };
            context.Stations.Add(station);

            station = new Station() { Town = "Poznań", Province = "wielkopolskie" };
            context.Stations.Add(station);

            station = new Station() { Town = "Szczecin", Province = "zachodniopomorskie" };
            context.Stations.Add(station);

            context.SaveChanges();
            base.Seed(context);
        }
    }
}
