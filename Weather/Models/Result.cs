﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Weather.Models
{
    public class Result
    {
        //[Key]
        public int Id { get; set; }

        [ForeignKey("Quantity")]
        public int QuantitytId { get; set; }
        public Quantity Quantity { get; set; }

        [ForeignKey("Station")]
        public int StationId { get; set; }
        public Station Station { get; set; }

        public DateTime Date { get; set; }
        public int Value { get; set; }
    }
}
