﻿namespace Weather.Models
{
    public class Station
    {
        public int Id { get; set; }
        public string Town { get; set; }
        public string Province { get; set; }
    }
}
