﻿namespace Weather.Models.WeatherModels
{
    public class Wind
    {
        public double speed { get; set; }
        public double deg { get; set; }
    }
}
