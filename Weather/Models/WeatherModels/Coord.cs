﻿namespace Weather.Models.WeatherModels
{
    public class Coord
    {
        public double lon { get; set; }
        public double lat { get; set; }
    }
}
